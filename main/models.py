from django.contrib.auth.models import AbstractUser
from django.db import models

from mptt.models import MPTTModel, TreeForeignKey


# Create your models here.


class User(AbstractUser):
    pass


from akoikelov.djazz.models import AbstractModel


class Category(MPTTModel, AbstractModel):
    class Meta:
        verbose_name_plural = 'Category'
        verbose_name = 'Category'

    title = models.CharField(verbose_name='title', max_length=255, unique=False, null=False)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')

    def __unicode__(self):
        return str(self.title)


class Product(AbstractModel):
    class Meta:
        verbose_name_plural = 'Product'
        verbose_name = 'Product'

    title = models.CharField(verbose_name='title', max_length=255, unique=False, null=False)
    description = models.TextField(verbose_name='description', null=False)
    short_description = models.CharField(verbose_name='short_description', max_length=1000, unique=False, null=False)
    price = models.FloatField(verbose_name='price', null=False)
    discount = models.FloatField(verbose_name='discount', null=False, default=0)
    category = models.ForeignKey(verbose_name='category', to=Category, on_delete=models.CASCADE,
                                 related_name='products', null=False)
    hidden = models.BooleanField(verbose_name='hidden', null=False)
    main_image = models.ImageField(verbose_name='main_image', null=False)

    def __unicode__(self):
        return str(self.pk)

    @property
    def price_new(self):
        _price = round(self.price, 1)

        return round(_price - (_price * (self.discount / 100)))


class ProductGallery(AbstractModel):
    class Meta:
        verbose_name_plural = 'ProductGallery'
        verbose_name = 'ProductGallery'

    image = models.ImageField(verbose_name='image', null=False)
    product = models.ForeignKey(verbose_name='product', to=Product, related_name='gallery', null=False,
                                on_delete=models.CASCADE)

    def __unicode__(self):
        return str(self.pk)


class Cart(AbstractModel):
    class Meta:
        verbose_name_plural = 'Cart'
        verbose_name = 'Cart'

    user = models.ForeignKey(verbose_name='user', to=User, on_delete=models.CASCADE, related_name='carts', null=False)
    is_active = models.BooleanField(verbose_name='is_active', null=False)
    order = models.OneToOneField(verbose_name='cart', to='Order', on_delete=models.CASCADE, related_name='order',
                                null=True)

    def __unicode__(self):
        return str(self.pk)

    @property
    def total_sum(self):
        total = 0

        for i in self.items.all():
            total += i.bought_price * i.quantity

        return round(total)


class CartItem(AbstractModel):
    class Meta:
        verbose_name_plural = 'CartItem'
        verbose_name = 'CartItem'

    product = models.ForeignKey(verbose_name='product', to=Product, on_delete=models.CASCADE, related_name='cart_items',
                                null=False)
    cart = models.ForeignKey(verbose_name='cart', to=Cart, on_delete=models.CASCADE, related_name='items', null=False)
    quantity = models.IntegerField(verbose_name='quantity', null=False)
    bought_price = models.FloatField(verbose_name='bought_price', null=False)

    def __unicode__(self):
        return str(self.pk)


class Order(AbstractModel):
    class Meta:
        verbose_name_plural = 'Order'
        verbose_name = 'Order'

    first_name = models.CharField(verbose_name='first_name', max_length=255, unique=False, null=False)
    phone = models.CharField(verbose_name='phone', max_length=255, unique=False, null=False)
    address = models.CharField(verbose_name='address', max_length=1000, unique=False, null=False)
    total_sum = models.FloatField(verbose_name='total_sum', null=False)
    status = models.CharField(verbose_name='status', max_length=255, unique=False, null=False)

    def __unicode__(self):
        return str(self.pk)


class Favorite(AbstractModel):
    class Meta:
        verbose_name_plural = 'Favorite'
        verbose_name = 'Favorite'

    product = models.ForeignKey(verbose_name='product', to=Product, on_delete=models.CASCADE, related_name='favorites',
                                null=False)
    user = models.ForeignKey(verbose_name='user', to=User, on_delete=models.CASCADE, related_name='favorites',
                             null=False)

    def __unicode__(self):
        return str(self.pk)


class ProductComment(AbstractModel):
    class Meta:
        verbose_name_plural = 'ProductComment'
        verbose_name = 'ProductComment'

    product = models.ForeignKey(verbose_name='product', to=Product, on_delete=models.CASCADE, related_name='product_comments',
                                null=False)
    user = models.ForeignKey(verbose_name='user', to=User, on_delete=models.CASCADE, related_name='product_comments',
                             null=False)
    text = models.TextField(verbose_name='text', null=False)

    def __unicode__(self):
        return str(self.pk)
