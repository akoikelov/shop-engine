# Generated by Django 2.2.2 on 2019-06-04 08:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_favorite_productcomment'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='discount',
            field=models.FloatField(default=0, verbose_name='discount'),
        ),
    ]
