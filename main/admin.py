from django.contrib import admin

# Register your models here.
from mptt.admin import DraggableMPTTAdmin

from .models import *


class UserAdmin(admin.ModelAdmin):
    list_display = ['date_joined', 'is_active', 'is_staff', 'email', 'last_name', 'first_name', 'username', 'is_superuser', 'last_login', 'password', 'id', ]
    list_filter = ['date_joined', 'is_active', 'is_staff', 'email', 'last_name', 'first_name', 'username', 'is_superuser', 'last_login', 'password', 'id', ]


admin.site.register(User, UserAdmin)


admin.site.register(Category, DraggableMPTTAdmin)


class ProductAdmin(admin.ModelAdmin):
    list_display = ['main_image', 'hidden', 'category', 'discount', 'price', 'short_description', 'description', 'title', 'updated_at', 'created_at', 'id', ]
    list_filter = ['main_image', 'hidden', 'category', 'discount', 'price', 'short_description', 'description', 'title', 'updated_at', 'created_at', 'id', ]


admin.site.register(Product, ProductAdmin)


class ProductGalleryAdmin(admin.ModelAdmin):
    list_display = ['product', 'image', 'updated_at', 'created_at', 'id', ]
    list_filter = ['product', 'image', 'updated_at', 'created_at', 'id', ]


admin.site.register(ProductGallery, ProductGalleryAdmin)


class CartAdmin(admin.ModelAdmin):
    list_display = ['is_active', 'user', 'updated_at', 'created_at', 'id', ]
    list_filter = ['is_active', 'user', 'updated_at', 'created_at', 'id', ]


admin.site.register(Cart, CartAdmin)


class CartItemAdmin(admin.ModelAdmin):
    list_display = ['bought_price', 'quantity', 'cart', 'product', 'updated_at', 'created_at', 'id', ]
    list_filter = ['bought_price', 'quantity', 'cart', 'product', 'updated_at', 'created_at', 'id', ]


admin.site.register(CartItem, CartItemAdmin)


class OrderAdmin(admin.ModelAdmin):
    list_display = ['status', 'total_sum', 'address', 'phone', 'first_name', 'updated_at', 'created_at', 'id', ]
    list_filter = ['status', 'total_sum', 'address', 'phone', 'first_name', 'updated_at', 'created_at', 'id', ]


admin.site.register(Order, OrderAdmin)

