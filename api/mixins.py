from main.models import Cart


class CartMixin(object):

    def get_cart(self):
        cart, _ = Cart.objects.get_or_create(user=self.request.user, is_active=True)
        return cart


class CartItemMixin(object):

    def check_item_exists(self, cart, product):
        return cart.items.filter(product=product).first()