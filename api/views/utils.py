from rest_framework.generics import ListAPIView

from api.serializers import CategorySerializer
from main.models import Category


class CategoryTreeView(ListAPIView):

    """
        Дерево категорий
    """

    queryset = Category.objects.filter(parent=None)
    serializer_class = CategorySerializer
    pagination_class = None