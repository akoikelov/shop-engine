from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAuthenticated

from api.serializers import CommentCreateSerializer
from main.models import ProductComment


class CommentCreateView(CreateAPIView):

    """
        Добавление комментария
    """

    serializer_class = CommentCreateSerializer
    queryset = ProductComment.objects.all()
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)