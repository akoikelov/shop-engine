from rest_framework.generics import CreateAPIView, DestroyAPIView, RetrieveAPIView
from rest_framework.permissions import IsAuthenticated

from api.mixins import CartMixin, CartItemMixin
from api.serializers import CartItemCreateSerializer, CartSerializer
from main.models import CartItem


class CartItemCreateView(CreateAPIView, CartMixin, CartItemMixin):

    """
        Добавление товара в корзину
    """

    serializer_class = CartItemCreateSerializer
    queryset = CartItem.objects.all()
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        product = serializer.validated_data['product']
        quantity = serializer.validated_data['quantity']
        replace_quantity = serializer.validated_data['replace_quantity']

        cart = self.get_cart()
        cart_item = self.check_item_exists(cart=cart, product=product)

        if cart_item:
            if replace_quantity:
                cart_item.quantity = quantity
            else:
                cart_item.quantity += quantity

            cart_item.save()
        else:
            serializer.save(cart=cart, bought_price=product.price_new)


class CartItemDeleteView(DestroyAPIView):

    """
        Удаление товара из корзины
    """

    permission_classes = (IsAuthenticated, )
    queryset = CartItem.objects.all()

    def perform_destroy(self, instance):
        cart = instance.cart
        super(CartItemDeleteView, self).perform_destroy(instance)

        order = cart.order

        if order:
            order.total_sum = cart.total_sum
            order.save()


class CartView(RetrieveAPIView, CartMixin):

    """
        Данные о корзине
    """

    serializer_class = CartSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return self.get_cart()