from rest_framework.exceptions import ValidationError
from rest_framework.generics import CreateAPIView, DestroyAPIView, get_object_or_404
from rest_framework.permissions import IsAuthenticated

from api.serializers import FavoriteCreateSerializer
from main.models import Favorite


class FavoriteCreateView(CreateAPIView):

    """
        Добавление в избранное
    """

    serializer_class = FavoriteCreateSerializer
    queryset = Favorite.objects.all()
    permission_classes = (IsAuthenticated, )

    def perform_create(self, serializer):
        self.check_exists(serializer)
        serializer.save(user=self.request.user)

    def check_exists(self, serializer):
        validated_data = serializer.validated_data

        if self.request.user.favorites.filter(product=validated_data['product']).exists():
            raise ValidationError({
                'custom': ['Товар уже добавлен в избранное']
            })


class FavoriteDeleteView(DestroyAPIView):

    """
        Удаление из избранного
    """

    permission_classes = (IsAuthenticated, )

    def get_object(self):
        return get_object_or_404(Favorite, product__id=self.kwargs['product_id'],
                                 user=self.request.user)