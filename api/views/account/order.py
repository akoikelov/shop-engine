from rest_framework.exceptions import ValidationError
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAuthenticated

from api.mixins import CartMixin
from api.serializers import OrderCreateSerializer
from main.models import Order


class OrderCreateView(CreateAPIView, CartMixin):

    """
        Создание заказа
    """

    serializer_class = OrderCreateSerializer
    queryset = Order.objects.all()
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        cart = self.get_cart()

        if cart.items.count() == 0:
            raise ValidationError({
                'custom': ['Корзина пуста. Невозможно создать заказ']
            })

        instance = serializer.save(total_sum=cart.total_sum)

        cart.order = instance
        cart.is_active = False
        cart.save()