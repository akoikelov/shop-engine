from django.urls import path, include
from rest_framework.routers import DefaultRouter
from rest_framework_swagger.views import get_swagger_view

from api.views.account.cart import CartItemCreateView, CartItemDeleteView, CartView
from api.views.account.comment import CommentCreateView
from api.views.account.favorite import FavoriteCreateView, FavoriteDeleteView
from api.views.account.order import OrderCreateView
from api.views.utils import *

app_name = 'api'

schema_view = get_swagger_view(title='Pastebin API')

router = DefaultRouter()

urlpatterns = [
    path('', include(router.urls)),
    path('docs/', schema_view),
    path('rest-auth/', include('rest_auth.urls')),
    path('rest-auth/registration/', include('rest_auth.registration.urls')),

    path('utils/', include([
        path('categories', CategoryTreeView.as_view())
    ])),
    path('favorite/', include([
        path('create/', FavoriteCreateView.as_view()),
        path('<int:product_id>/', FavoriteDeleteView.as_view())
    ])),
    path('comment/create/', CommentCreateView.as_view()),
    path('cart/', include([
        path('', CartView.as_view()),
        path('item/create/', CartItemCreateView.as_view()),
        path('item/<int:pk>/', CartItemDeleteView.as_view())
    ])),
    path('order/create/', OrderCreateView.as_view())
]
