from rest_framework.fields import BooleanField, SerializerMethodField
from rest_framework.serializers import ModelSerializer, Serializer

from main.models import *


class RecursiveField(Serializer):
    def to_representation(self, value):
        serializer = self.parent.parent.__class__(value, context=self.context)
        return serializer.data


class CategorySerializer(ModelSerializer):
    children = RecursiveField(many=True)

    class Meta:
       model = Category
       fields = ("id", "title", "children",)


class FavoriteCreateSerializer(ModelSerializer):
    class Meta:
       model = Favorite
       exclude = ('user', )


class CommentCreateSerializer(ModelSerializer):
    class Meta:
        model = ProductComment
        fields = ('text', 'product')


class CartItemCreateSerializer(ModelSerializer):
    class Meta:
       model = CartItem
       exclude = ('cart', 'created_at', 'updated_at', 'bought_price')

    replace_quantity = BooleanField(required=False, default=False)

    def create(self, validated_data):
        if 'replace_quantity' in validated_data:
            del validated_data['replace_quantity']

        return super(CartItemCreateSerializer, self).create(validated_data)


class OrderCreateSerializer(ModelSerializer):
    class Meta:
        model = Order
        exclude = ('total_sum', 'status',)


class CartItemDetailSerializer(ModelSerializer):
    class Meta:
        model = CartItem
        exclude = ('created_at', 'updated_at', 'cart')


class CartSerializer(ModelSerializer):
    items = SerializerMethodField()
    total_sum = SerializerMethodField()

    class Meta:
        model = Cart
        exclude = ('created_at', 'updated_at',)

    def get_total_sum(self, obj):
        return obj.total_sum

    def get_items(self, instance):
        serializer = CartItemDetailSerializer(many=True, data=instance.items.all().order_by('-created_at'),
                                              context=self.context)
        serializer.is_valid()

        return serializer.data